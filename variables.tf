variable "project_id" {
  description = "project id"
}

variable "region" {
  description = "region"
}

variable "gitlab_token" {
  description = "The gitlab token for runners"
  type        = string
}