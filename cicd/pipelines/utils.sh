#!/bin/bash

function exportDev {
    echo "entering in develop, master or feature/ci envVars"
    export TAG="latest"
    export NAMESPACE="intuat"
    export SPRING_PROFILE="dev"
    export HELM_VALUES_FILE="values.yaml"
}

function exportRelease {
    echo "entering in release envVars"
    export TAG=$(echo ${CI_COMMIT_REF_NAME} | cut -d '/' -f 2)
    export NAMESPACE="prod"
    export SPRING_PROFILE="prod"
    export HELM_VALUES_FILE="values-prod.yaml"
}

function logEnvsParsed {
    echo "TAG PARSED: ${TAG}"
    echo "NAMESPACE PARSED: ${NAMESPACE}"
    echo "SPRING_PROFILE PARSED: ${SPRING_PROFILE}"
    echo "HELM_VALUES_FILE PARSED: ${HELM_VALUES_FILE}"
}

function setEnvVars {
    echo "CI_COMMIT_REF_NAME: ${CI_COMMIT_REF_NAME}"
    if [[ ${CI_COMMIT_REF_NAME} == *"develop"* ]] || [[ ${CI_COMMIT_REF_NAME} == *"master"* ]] || [[ ${CI_COMMIT_REF_NAME} == *"feature"* ]] ; then
        exportDev
    else
        exportRelease
    fi
    logEnvsParsed
}

function setEnvVarsSh {
    echo "REF_NAME: ${CI_COMMIT_REF_NAME}"
    if [ "${CI_COMMIT_REF_NAME}" != "${CI_COMMIT_REF_NAME#develop}" ] || [ "${CI_COMMIT_REF_NAME}" != "${CI_COMMIT_REF_NAME#master}" ] || [ "${CI_COMMIT_REF_NAME}" != "${CI_COMMIT_REF_NAME#feature}" ]
    then
        exportDev
    else
        exportRelease
    fi
    logEnvsParsed
}