# Requirements

* [Install GCloud Client](https://cloud.google.com/sdk/docs/install)
* Login to your google account
```
gcloud auth application-default login
```
* [Install Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
* Init this project dependencies
```
terraform init
```

This solution is splitted in four gitlab repositories inside the same Group [Nuvolar Test](https://gitlab.com/nuvolarTest).


# Part 1

The test:

```
In this part of the test we want you to design a CI/CD strategy for a described system. We will evaluate your proposed solution and the toolchain selected.

System description:


- We have an HTTP API application (no frontend) composed by six Java microservices
- The microservices are Dockerized
- The microservices communicate to each other using RabbitMQ queues, so we need a RabbitMQ instance
- Some microservices are stateless and some others manage data using dedicated MySQL schemas, so we need a MySQL Cluster
- Concurrency in the system is high, we expect several users consuming the API at the same time
```

## Introduction

For this first part of the test, I will explain some concepts for better and furhter explanations of the solution.

## Branching Model

In a DevOps or SW lifecycle, everything (almost) should start from an original repository of code. This code should contain the necessary to build and deliver or deploy a component into an environmnt. Some premises from the [Twelve Factors App Priciples](https://12factor.net/).

So, I will define, for the three SpringBoot based components, a [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) branching model, where several branches will trigger the CI/CD pipeline.

The affected branches that will trigger the pipelines are:

### Master:
* This branch will have the last and most stable version of my component, for example, the latest production version. And there we will only have those changes.
* This branch should be Tagged with every new stable or production version for easily tracking things or rolback processes.
* This branch is protected. Only a Pull Request will be accepted.
* The Sync between other branches and this one, should be *manually* performed and only by a Release Manager (or several).

### Develop: 
* There we will run the HOLE flow until the Deployment Stage. We will generate overwritable versions and deploy them on every change to ensure that everything is still working fine. i.e: SNAPSHOT for maven, "latest" for docker, etc.
* This change will only be triggered on a PR opened from a *feature* branch to the target *develop* branch. If everything goes well, the Pull Request will be merged automatically or manually to the target branch.
* This branch will have all the *most recent* changes.

### Feature
* Every new change or feature to our component MUST be as atomic as possible. Smaller changes, smaller bugs, smaller future problems and better tracking of our systems. So, every change should start on a new feature branch.
* Like the develop branch, every change to this branch *may* trigger a hole CI/CD process. This may be elegible by each application owner as a flag or something like that.

### Release
* This branch will start from the *develop* branch and will start the ReleaseCandidate version process and validations.
* If we will apply a Continuous Deployment into the production environments, then, we will perform the deploy. But I will recommend to stop on a Continuous Delivery flow for a release version, end perform the deploy to a production environment outside this pipelines.

Next is an example of for the develop and feature branch. For the Release and master branch will be almost the same:
![Branch Model](doc/bmodel.JPG)

## CI/CD Stack proposal

For the solution, I will be using Gitlab. Gitlab for the scm and for the CI/CD processes too. I really like how Gitlab is achieving and exposing all the DevOps tools inside tehir platform. 1 repo -> 1 component -> 1 CI/CD process (another premise from the [Twelve Factors App Priciples](https://12factor.net/)).

Said that, I will explain how the pipelines will be implemented AD-HOC only for this solution. It needs to be clarified that this implementation is not implenented thinking of escalability, maintenance or reusability at all, nor the branching model proposed as a fine solution for each component, as I will have no source code nor more detailed information. I will implement a pipeline for each component that will trigger only for develop and a release branch, without a Pull Request and performing Continuous Deployment for every environment.

The CI/CD process will have the same Stages, or steps for every component. Each component, depending on his tech stack, kind, purpose or final need, will implenent all or only a few Steps of this Software lifecycle.

* **Validation**: All components can validate things needed for the rest of the pipeline (inputs, some values, etc). Blocks on failure.
* **Build**: Builds (Compile) the code the src code. Blocks on failure
* **Test**: Executes the unit testing. Blocks on failure
* **Quality**: This step has two parallel steps:
    * **Sonar**: Executes Quality tests against Sonar. Both code analysis and Quality Gates. May break on failure.
    * **Security**: Executes security validations. Needs some kind of third party SW. May break on failure.
* **Package**: Packages the component (jar, war, tgz, zip, ipa). Blocks on failure.
* **Store**: Stores the artifact into a Binary Repository (Nexus, Artifactory, Gitlab, etc). Blocks on failure.
* **Provision**: If needed, provisions infrastructure or services for the E2E tests, Deploy or for generating some kind of detailed reports. Blocks on failure.
* **Deploy**: Deploys the artifact into the target environment. Blocks on failure.
* **E2E**: Executes some kind of E2E tests (Selenium, Fmaps, HttpRequests, etc). Blocks on failure.
* **Deprovision**: Deproviosion of the infrastructure or services during the provision stage. Blocks on failure.
* **Results**: Generates Show some reporting of the process, shows it or stores it somewhere. Blocks on failure.

As described above, this may be a complete SDLC for the CI/CD processes. It may satisfy almost every kind of technology, as we are going to implement each stage different for each technology.

So, for the solution, I will assume that the database service is already deployed or available on each environmen. This assumption is because of the "cluster" word as a requirement. If possible, we should be working with a SaaS DB and leave all this configuration to the end provider, like a Cloud SQL Regional Replication, or directly a failover database cluster with a load balancing. Depends which one fits best for the real case.

### Pipelines

I will create a pipeline that implements some stages defined above for a microservice and propose another for the database migrations.

The pipelines will inherit the implementation, so that the Architecture/DevOps Team will be the only team for maintaining the implementation.

The code for the pipelines is located unded *cicd/pipelines* folder.

For the pipelines, I will have a tiny ShellScript for some utils functions, a main *.yml for each *kind* of technology, a vars.yml for defining common variahbles for the pipeliens and all the stages implementation inside the *cicd/pipelines/stages* folder.

![Tree](doc/treeCicd.JPG)

For viewing or launching a pipeline for a component, just navigate to: CI/CD -> Pipelines:

![Tree](doc/pipes.JPG)

### Pipeline for a Java Microservice

As I only have the docker image name for this test, I will only implement the deploy stage.

```yaml
include:
  - project: 'nuvolarTest/nuvolar-test'
    file: '/cicd/pipelines/vars.yml'
  - project: 'nuvolarTest/nuvolar-test'
    file: '/cicd/pipelines/stages/k8s-deploy.yml'

stages:
  #- validate
  #- build
  #- test
  #- qa
  #- package
  #- store
  #- provision
  - deploy
  #- e2e
  #- deprovision
  #- results
```
And the Deploy Stage:

```yaml
variables:
  HELM_EXTRA_VARS: ""

k8s-deploy:
  tags:
    - docker
  stage: deploy
  image: ${HELM_CI_REPO}:${HELM_CI_VERSION}
  only:
    - develop
    - /^release.*$/
  before_script:
    - git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@${UTILS_REPO}
    - source ${UTILS_PATH}/cicd/pipelines/utils.sh
    - setEnvVarsSh
    - echo ${HELM_EXTRA_VARS}
  script:
    - sed -i "s/latest/${TAG}/g" ${UTILS_PATH}/cicd/helm/${TYPE}/Chart.yaml
    - helm upgrade
      --namespace ${NAMESPACE}-${NAMESPACE_SUFFIX}
      --create-namespace
      --install
      --wait
      --debug
      --set name=${CI_PROJECT_NAME}
      --set springProfilesActive=${SPRING_PROFILE}
      ${HELM_EXTRA_VARS}
      ${CI_PROJECT_NAME}
      ${UTILS_PATH}/cicd/helm/${TYPE}
      -f ${UTILS_PATH}/cicd/helm/${TYPE}/${HELM_VALUES_FILE}
```

So, for executing this pipelines we will only need a *.gitlab-ci.yml* file on the root path of our repository, with only two things:

* The include to the pipeline itself.
* An optional **HELM_EXTRA_VARS** definition to overload some helm values during the deploy.

So, for each component of the test, this will be the *.gitlab-ci.yml*:

```yaml
variables:
  HELM_EXTRA_VARS: "--set image.name=${CI_PROJECT_NAME}"

include:
    - project: 'nuvolarTest/nuvolar-test'
      file: '/cicd/pipelines/microservice-pipeline.yml'
```

It is a very tiny file, with no logic at all, and we will overloading the docke rimage name matching the name of the repository. Then, perform the deploy to the kubernetes environment.

### Pipeline for a Database Migration as code

I would implement a pipeline for the database migrations by code. I would use [FlyWay](https://flywaydb.org/) or [Liquibase](https://www.liquibase.org/) for more complexes or hugre database environments.

This pipeline could be directly implemented as another Step for the Java Library that defines all the JPA Entities for example. It is easy to configure inside the pom for each environment and calling it from the pipeline as another maven goal. But, for the solution, it is out of the scope.

### Helm
We will use Helm (3.X) as the main tool for managing deployments into a kubernetes cluster. It will bring us this abstraction layer for the deployment Stage, as we will be able to deploy each component, at the same manner, just changing the tarjet kubernetes cluster with no ease.

Later I will talk about the environments, but for the test, I have implemented two. So we have two different files for each environment, with syntax *values-&#60;environment&#62;.yml*, and the default *values.yml*

![Tree](doc/helm.JPG)

### RabbitMQ

On part 2 I will talk about Terraform, but a RabbitMQ Chart is deployed into the cluster. But as I have no more information about this component, it is deployed with standard configuration inside the *private architecture namespace*.

### Concurrency
Each deployment has configured autoscaling (with default values). So on high traffic it will autoscale. The node pools of the cluster have autoscaling enabled too if the resources are running low for the underlying deployments.

# Part 2
```
In this part of the test we want to see some code, to evaluate your ability to create an infrastructure.

Task:

We have very simple API that we want to deploy into the Cloud. The system consists on three stateless Spring Boot microservices. Docker images for the microservices can be found here:

    https://hub.docker.com/r/nuvolar/api-gateway
    https://hub.docker.com/r/nuvolar/order-service
    https://hub.docker.com/r/nuvolar/customer-service

The three microservices talk to each other via HTTP. The api-gateway microservice should be accesible publicly (performing an HTTP GET request to /order endpoint should return 200 OK status code).

Requirements:

    You can create the solution in any language or framework of your choice
    The infrastructure provider can be either Google Cloud or AWS using the technology you prefer
    Have the API exposed over SSL
    It is optional to deploy your solution, in any case you should provide complete and clear instructions for us to recreate your setup
```

## Architecture
I will deploy with terraform the following architecture into GCP:

![Tree](doc/diagram.JPG)

> NOTE: originally I tried to deploy an *Autopilot* GKE Cluster, but the version is not mature and was experiencing some problems.

## Terraform
I have used terraform to automate and keep everything (as possible) as IaaC. Some points must be manually performed, but I will talk later about that, because I don't have all the details needed for a complete infrastructure configuration.

I have implemented two modules with terraform:

* gke: this module will deploy every service on the GKE:
    * The VPC and subnet
    * The public static IP
    * the cluster and a node pool
* services: all the related kubernetes configuration and services for the test:
    * Grab the kubeconfig
    * Nginx Ingress Controller
    * Jet Stack Cert Manager
    * RabbitMQ
    * Gitlab Runners: We will auto configure the gitlab runners for our pipelines inside the cluster.
* a *main.tf* as entry point, where we will deploy an additional "netutils" pod for networking things on the cluster, and the ClusterIssuer for the SSL certificates and Lets Encrypt.

![Tree](doc/tefTree.JPG)

> NOTE: You will need to update the values of the *terraform.tfvars* file to point to your google project and region.

### Ingress, NginX and Lets Encrypt
I have deployed a Nginx Ingress Controller in the cluster and the JetStack Cert Manager for auto provision and serve the SSL certificates for each domain configured for our public endpoints.

So, for this solution, the api-gateway will be available at some *sslip.io* domain, depending on the public static ip, like *35.187.119.127.sslip.io*.

### Environments
I have configured the deployment to autoprovision two environments as two different namespaces:

* INTUAT: the commonly known, INT or Development environment, plus the QA environment.
* PROD: the production environment.

Every deployment will create its own namespace if not exists. Then, Rolling Update its deployment.

![Tree](doc/workloads.JPG)

### NOTES
* I have not been able to get a 200 code from the suggested api gateway endpoint on the test. I have seen some logs with an stack trace returning a 500 Internal Server Error, but with no more information I was not able to mitigate the error. Maybe some missing springboot config mounted? Some RabbitMQ extra conf somewhere like env vars?
![Tree](doc/error1.JPG)
![Tree](doc/error2.JPG)
* I was using the *sslip.io* domain, like *xip.io* (but the last one was KO during the test!!!) for securize the public endpoint.


### Replicate this solution

All the four repositories are hosted as public repositories inside Gitlab. You can clone them, but you will need this considerations:

* You will need to update in each helm values file, the value of the static ip when provisioning the cluster:
![Tree](doc/staticIp.JPG)
* You will need to store all the repositories on a Gitlab Group (not needed but easy configuration).
* You will need to "copy-paste" the kubeconfig content inside a new "File" variable on the Gitlab Group: Settings -> CI/CD -> Variables. Important naming it as **KUBECONFIG** (for the CI/CD process grabbing automatically the k8s conf) and mark it as **Non protected**.
![Tree](doc/variable.JPG)
* You will need to inform the gitlab token for autoconfigure the gitlab runners at the Group and disable shared runners at: Settings -> CI/CD -> Runners. Then, set this value in the terraform vars file *terraform.tfvars*.
![Tree](doc/runners.JPG)

* If the terraform apply command exists with some kind of timeout error or not available to reach the cluster, just relaunch it. I have exprienced some errors like that but had not enough time to investigate the possible root cause.
* Remember to install the *gcloud* and *terraform* clients.
* Remember to update the terraform values file.

A possible terraform networking issue:
![Tree](doc/tfError.JPG)