output "region" {
  value       = var.region
  description = "GCloud Region"
}

output "project_id" {
  value       = var.project_id
  description = "GCloud Project ID"
}

output "kubernetes_cluster_name" {
  value       = module.gke.kubernetes_cluster_name
  description = "GKE Cluster Name"
}

output "kubernetes_cluster_host" {
  value       = module.gke.kubernetes_cluster_host
  description = "GKE Cluster Host"
}

output "kubernetes_cluster_ca_certificate" {
  value       = module.gke.kubernetes_cluster_ca_certificate
  description = "Base64 GKE Cluster Client CA Cert"
}

output "static_ip" {
  value       = module.gke.static_ip
  description = "Load Balancer Static IP"
}