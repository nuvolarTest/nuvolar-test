terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.66.0"
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.1.2"
    }
    null = {
      source = "hashicorp/null"
      version = "3.1.0"
    }
    kubectl = {
      source = "gavinbunney/kubectl"
      version = "1.10.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.1.0"
    }
  }

  required_version = "~> 0.14"
}

