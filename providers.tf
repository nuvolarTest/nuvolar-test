provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

provider "google" {
  project = var.project_id
  region  = var.region
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

provider "kubectl" {
  config_path = "~/.kube/config"
  apply_retry_count = 15
}
