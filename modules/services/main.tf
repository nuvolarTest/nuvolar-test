resource "null_resource" "deploy_charts" {
   provisioner "local-exec" {
    command = "gcloud container clusters get-credentials ${var.cluster_name} --zone ${var.region}"
  }
}

resource "helm_release" "cert-manager" {
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  verify = false
  namespace = "cert-manager"
  create_namespace = true
  skip_crds = false

  set {
    name  = "installCRDs"
    value = true
  }

  depends_on       = [
    null_resource.deploy_charts
  ]
}

resource "helm_release" "ingress-nginx" {
  name       = "ingress-nginx"

  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  namespace = "ingress-nginx"
  create_namespace = true
  verify = false

  set {
    name  = "controller.service.loadBalancerIP"
    value = var.static_ip
  }

  set {
    name  = "rbac.create"
    value = true
  }

  depends_on       = [
    null_resource.deploy_charts
  ]
}

resource "helm_release" "rabbitmq" {
  name       = "rabbitmq"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "rabbitmq"
  verify = false
  namespace = "architecture"
  create_namespace = true

  depends_on = [
    null_resource.deploy_charts
  ]
}

resource "kubernetes_namespace" "gitlab" {
  metadata {
    name = "gitlab"
  }

  depends_on = [
    null_resource.deploy_charts
  ]
}

resource "helm_release" "gitlab-runner" {
  name       = "gitlab-runner"
  repository = "https://charts.gitlab.io"
  chart      = "gitlab-runner"
  verify = false
  namespace = "gitlab"

  set {
    name  = "gitlabUrl"
    value = "https://gitlab.com/"
  }

  set {
    name  = "runnerRegistrationToken"
    value = var.gitlab_token
  }

  set {
    name  = "runners.tags"
    value = "docker"
  }

  depends_on = [
    kubernetes_namespace.gitlab
  ]
}

resource "kubernetes_cluster_role_binding" "gitlab" {
  metadata {
    name = "gitlab-edit"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "edit"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "default"
    namespace = "gitlab"
  }

  depends_on = [
    kubernetes_namespace.gitlab
  ]
}