variable "static_ip" {
  description = "The static ip the LoadBalancer"
  type        = string
}

variable "region" {
  description = "The region for the network. If the cluster is regional, this must be the same region. Otherwise, it should be the region of the zone."
  type        = string
}

variable "cluster_name" {
  description = "The gke cluster name"
  type        = string
}

variable "cluster_ip" {
  description = "The gke cluster ip"
  type        = string
}

variable "gitlab_token" {
  description = "The gitlab token for runners"
  type        = string
}