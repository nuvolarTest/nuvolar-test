variable "project_id" {
  description = "The project ID where all resources will be launched."
  type        = string
}

variable "region" {
  description = "The region for the network. If the cluster is regional, this must be the same region. Otherwise, it should be the region of the zone."
  type        = string
}

variable "gke_num_nodes" {
  default     = 1
  description = "number of gke nodes"
}