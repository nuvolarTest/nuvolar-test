# VPC
resource "google_compute_network" "vpc" {
  name                    = "${var.project_id}-vpc"
  auto_create_subnetworks = "false"
}

# Subnet
resource "google_compute_subnetwork" "subnet" {
  name          = "${var.project_id}-subnet"
  region        = var.region
  network       = google_compute_network.vpc.name
  ip_cidr_range = "10.10.0.0/24"
}

# static ip address
resource "google_compute_address" "static" {
  name = "ipv4-address"
}

# GKE cluster
resource "google_container_node_pool" "node_pool" {
  name     = "main-pool"
  project  = var.project_id
  location = var.region
  cluster  = google_container_cluster.primary.name
  node_count = var.gke_num_nodes

  node_config {
    image_type   = "COS"
    machine_type = "n1-standard-1"
    preemptible  = false

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}

resource "google_container_cluster" "primary" {
  name     = "${var.project_id}-gke"
  location = var.region

  initial_node_count       = var.gke_num_nodes

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true

  network    = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name
}

/*
IAM SA for managing deployments with helm

NOT REALLY NEEDED FOR THE TEST
*/
/*
resource "google_service_account" "service_account" {
  account_id   = "cicd-sa"
  display_name = "cicd Service Account"
}

# ----------------------------------------------------------------------------------------------------------------------
# ADD ROLES TO SERVICE ACCOUNT
# ----------------------------------------------------------------------------------------------------------------------
locals {
  all_service_account_roles = [
    "roles/editor"
  ]
}

resource "google_project_iam_member" "service_account-roles" {
  for_each = toset(local.all_service_account_roles)

  project = var.project_id
  role    = each.value
  member  = "serviceAccount:${google_service_account.service_account.email}"
}

resource "google_service_account_key" "cicd_key" {
  service_account_id = "cicd-sa"

  depends_on = [
    google_project_iam_member.service_account-roles
  ]
}*/